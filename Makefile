#!make

DOCKER_IMAGE ?= devcontainers/common/devcontainer:dev

all: build

include .shared/vault.mk

.PHONY: requirements
requirements:
	pip3 --disable-pip-version-check install -r requirements.txt
	@echo "killing vscode-pylance so it restarts and picks up new libs"
	@kill $(shell pgrep -f 'node.*vscode-pylance') 2>/dev/null || true

.PHONY: build
build:
	./app.py

.PHONY: publish
publish:
	devcontainer features publish -r ghcr.io -n geekstuff-dev/devcontainers-features features/

.PHONY: cleanup
cleanup:
	rm -rf features
