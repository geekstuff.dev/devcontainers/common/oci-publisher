variable "gitlab_project_id" {
  type = number
}

variable "oci_secrets_path" {
  type = string
}
