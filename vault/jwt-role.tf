resource "vault_jwt_auth_backend_role" "role" {
  backend         = "jwt_gitlab"
  bound_audiences = ["https://vault.geekstuff.dev/auth/jwt_gitlab/login"]
  bound_claims = {
    project_id    = var.gitlab_project_id,
    ref_protected = true,
  }
  role_name              = "devcontainers_oci-publisher"
  role_type              = "jwt"
  token_explicit_max_ttl = 10
  token_policies = [
    vault_policy.secrets.name,
  ]
  user_claim = "iss"
}
