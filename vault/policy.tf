resource "vault_policy" "secrets" {
  name   = "devcontainers_oci-publisher_secret"
  policy = data.vault_policy_document.secrets.hcl
}

data "vault_policy_document" "secrets" {
  rule {
    path         = "kv/data/${var.oci_secrets_path}"
    capabilities = ["read"]
  }
}
