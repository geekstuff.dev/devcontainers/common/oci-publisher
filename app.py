#!/usr/local/bin/python

import sys
sys.path.append('src/')
import features as feats, preparer as prep, collection

def main():
    print("Starting [{app_name}]\n".format(app_name="oci-publisher"))
    features = feats.Features(62306765)
    preparer = prep.Preparer(features)
    preparer.prepare()
    readme = collection.Readme(features)
    readme.generate()
    wrapUp = collection.WrapUp()
    if wrapUp.has_updates():
        wrapUp.commit_push()
    else:
        wrapUp.commit_push_empty()

if __name__ == '__main__':
    sys.exit(main())
