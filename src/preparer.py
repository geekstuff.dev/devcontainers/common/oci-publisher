import features as feats, os, fileinput
from git.repo import Repo
from distutils.dir_util import copy_tree

class Preparer:
    top_dir = "features"
    generated_dir = "features/src"
    collection_repo = "https://github.com/geekstuff-dev/devcontainers-features.git"

    def __init__(self, features: feats.Features):
        self.features = features

    def cleanup(self, dir: str):
        for root, dirs, files in os.walk(dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

    def prepare(self):
        self.cleanup(self.top_dir)
        _ = Repo.clone_from(self.collection_repo, self.top_dir)
        os.makedirs(self.generated_dir, exist_ok=True)
        self.clone_features()

    def clone_features(self):
        for feature in self.features.iterator():
            print("Cloning project [{prj}]".format(prj=feature.name()))
            self.cleanup("feature-tmp")
            _ = Repo.clone_from(feature.project.http_url_to_repo, "feature-tmp"),
            repo = Repo("feature-tmp")
            latest = repo.create_head("latest", feature.highest_protected_tag.name)
            repo.head.reference = latest
            repo.head.reset(index=True, working_tree=True)
            #
            copy_tree("feature-tmp/src", "{x}/{y}".format(x=self.generated_dir, y=feature.path()))
            with fileinput.FileInput(
                "{x}/{y}/devcontainer-feature.json".format(x=self.generated_dir, y=feature.path()),
                inplace=True,
            ) as file:
                for line in file:
                    print(line.replace("0.0.0", feature.highest_protected_tag.name[1:]), end='')
        self.cleanup("feature-tmp")
        os.rmdir("feature-tmp")
