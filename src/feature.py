import gitlab.v4.objects.projects as gitlab_project
from datetime import datetime
from pkg_resources import parse_version as version

datetime_format = r"%Y-%m-%dT%H:%M:%S.%f%z"

class Feature:
    has_protected_tag = False

    def __init__(self, gitlabProject: gitlab_project.Project):
        self.project = gitlabProject
        self.fetch_latest_tag()

    def is_eligible(self) -> bool:
        return self.has_protected_tag

    def name(self):
        return self.project.name

    def path(self):
        return self.project.path

    def fetch_latest_tag(self):
        for tag in self.project.tags.list(Iterator=True, get_all=True):
            if tag.protected == False:
                continue

            if self.has_protected_tag == False:
                self.highest_protected_tag = tag
                self.has_protected_tag = True
            else:
                if version(tag.name) > version(self.highest_protected_tag.name):
                    self.highest_protected_tag = tag
