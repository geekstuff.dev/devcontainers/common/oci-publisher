import features as feats, os, jinja2, shutil
from distutils.dir_util import copy_tree
from jinja2 import Environment, FileSystemLoader
from git.repo import Repo
from git.util import Actor

class Readme:
    target = "features/README.md"

    def __init__(self, features: feats.Features):
        self.features = features
        self.cleanup()
        self.templates = self.get_templates()

    def cleanup(self):
        if os.path.isfile(self.target):
            os.remove(self.target)

    def get_templates(self) -> dict[str, jinja2.Template]:
        environment = Environment(
            loader=FileSystemLoader("templates/"),
        )

        return {
            "readme": environment.get_template("README.md.jinja"),
        }

    def generate(self):
        print("Generate README")
        content = self.templates["readme"].render({
            "features": self.features.iterator(),
        })
        with open(self.target, mode="w", encoding="utf-8") as message:
            message.write(content)

        print(f"generated [{self.target}]")

class WrapUp:
    def __init__(self):
        self.repo = Repo("features")
        os.makedirs("features/.github/workflows", exist_ok=True)
        shutil.copyfile("templates/release.yaml", "features/.github/workflows/release.yaml")
        shutil.copyfile("templates/validate.yml", "features/.github/workflows/validate.yml")

    def has_updates(self):
        return self.repo.is_dirty(untracked_files=True)

    def commit_push(self):
        self.repo.index.add(["src", "README.md", ".github"])
        author = Actor("Geekstuff Dev", "as-gsdev@geekstuff.it")
        self.repo.index.commit(
            message="Update features from sources",
            author=author,
            committer=author,
        )
        origin = self.repo.remote(name='origin')
        origin.set_url("https://{user}:{token}@github.com/{namespace}.git"
            .format(user=os.environ.get("OCI_REG_USER"),token=os.environ.get("OCI_REG_PASS"),namespace=os.environ.get("OCI_REG_NAMESPACE")))
        origin.push()

    def commit_push_empty(self):
        author = Actor("Geekstuff Dev", "as-gsdev@geekstuff.it")
        self.repo.index.commit(
            message="Update features from sources (empty)",
            author=author,
            committer=author,
        )
        origin = self.repo.remote(name='origin')
        origin.set_url("https://{user}:{token}@github.com/{namespace}.git"
            .format(user=os.environ.get("OCI_REG_USER"),token=os.environ.get("OCI_REG_PASS"),namespace=os.environ.get("OCI_REG_NAMESPACE")))
        origin.push()
