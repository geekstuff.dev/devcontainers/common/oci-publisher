import gitlab, feature as feat
from collections import OrderedDict
from collections.abc import Generator

class Features:
    def __init__(self, gitlabGroupID: int):
        client = gitlab.Gitlab()
        group = client.groups.get(gitlabGroupID)
        projects = {}
        for each in group.projects.list(iterator=True,per_page=50,visibility="public"):
            project = client.projects.get(each.get_id())
            feature = feat.Feature(project)
            if feature.is_eligible():
                projects[feature.name()] = feature
            print("Found feature project [{prj}] with tag [{tag}]".format(prj=feature.name(),tag=feature.highest_protected_tag.name))

        self.projects = OrderedDict(sorted(projects.items(), key=lambda t: t[0]))

    def iterator(self) -> Generator[feat.Feature]:
        for project_key in self.projects:
            yield self.projects[project_key]
