# Geekstuff.dev / Devcontainers / Common / OCI Publisher

This project publishes the Devcontainers Features created in [this group](https://gitlab.com/geekstuff.dev/devcontainers/features)
using their latest protected tag, to an OCI container registry that supports the
devcontainer media type.

## How it works

Devcontainers Features is very recent and does not yet clearly state how
to have 1 git project per vscode feature. It very much seems to expect
a collection of features in a single git project.

Here we get around that by having a pipeline that loops our features,
picks their latest tag, clone them locally to temporary create that
collection of features, and finally uses the devcontainer cli to upload
those features and collection parts.
