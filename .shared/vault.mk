VAULT_OIDC_PORT ?= 8269

vault-login: vault-login-google

vault-login-google: OIDC_PATH ?= oidc_google
vault-login-google: .vault-login

vault-login-gitlab: OIDC_PATH ?= oidc_gitlab
vault-login-gitlab: .vault-login

.vault-login:
	@test -n "${OIDC_PATH}"
	@test -n "${VAULT_OIDC_PORT}"
	vault login -method=oidc -path=${OIDC_PATH} port=${VAULT_OIDC_PORT}
